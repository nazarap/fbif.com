import {Component, OnInit, ViewContainerRef} from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import News from '../../domain/News'
import Image from '../../domain/Image'
import {ApiService} from "../../services/api.service";

@Component({
  selector: 'main-page',
  templateUrl: './main-page.component.html',
  providers: [ApiService],
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  newsList: Array<News>;
  newsMoreList: Array<News>;
  adverList: Array<Image> = [];
  sponsorList: Array<Image>;
  accordions: Object;
  addNewsModal: any = {
    open: false,
    title: null,
    description: null,
    image: null
  };
  showNewsModal: any = {
    open: false,
    selected: null
  };
  addSponsorModal: any = {
    open: false,
    href: null
  };

  constructor(private meta: Meta, private apiService: ApiService, private title: Title) {
      // title.setTitle("FBIF");

      this.accordions = {};
      // meta.addTags([
      //     {
      //       name: 'description', content: 'Стилів архітектури. Знайти стиль архітектури по імені. Можливість здійснювати пошук архітектурних стилів по імені.'
      //     },
      // ])
  }

  ngOnInit(): void {
    this.getGameNews();
    this.getGeneralNews();
    this.getSponsors();
    this.apiService.advertisement().subscribe(
      data => {this.adverList = data as Image[]},
      error => {},
      () => {}
    );
  }

  getSponsors(): void {
    this.apiService.sponsors().subscribe(
      data => {this.sponsorList = data as Image[]},
      error => {},
      () => {}
    );
  }

  getGameNews(): void {
    this.apiService.newsGame().subscribe(
      data => {
        this.newsList = data as News[];
        this.newsList.reverse();
      },
      error => {},
      () => {}
    );
  }

  getGeneralNews(): void {
    this.apiService.newsGeneral().subscribe(
      data => {
        this.newsMoreList = data as News[];
        this.newsMoreList.reverse();
      },
      error => {},
      () => {}
    )
  }

  deleteNews(id, index): void {
    if (this.addNewsModal.type === 'GAME') {
      this.newsList.splice(index, 1);
    } else {
      this.newsMoreList.splice(index, 1);
    }
    this.apiService.deleteNews(id).subscribe(
      data => {
      },
      error => {},
      () => {}
    );
  }

  deleteSponsor(id, index): void {
    this.sponsorList.splice(index, 1);
    this.apiService.deleteSponsor(id).subscribe(
      data => {
      },
      error => {},
      () => {}
    );
  }

  createNewNews(): void {
    this.apiService.addNews({
      title: this.addNewsModal.title,
      description: this.addNewsModal.description,
      image: this.addNewsModal.image,
      category: this.addNewsModal.type || 'GAME'
    }).subscribe(
      data => {
        if(this.addNewsModal.type === 'GAME') {
          this.getGameNews();
        } else {
          this.getGeneralNews();
        }
        this.addNewsModal = {
          open: false,
          title: null,
          description: null,
          image: null
        };
      },
      error => {},
      () => {}
    )
    ;
  }

  createSponsor(): void {
    this.apiService.addSponsors({
      href: this.addSponsorModal.href,
      image: this.addNewsModal.image
    }).subscribe(
      data => {
        this.getSponsors();
        this.addNewsModal.image = null;
        this.addSponsorModal = {
          open: false,
          href: null
        };
      },
      error => {},
      () => {}
    )
    ;
  }

  handleFileSelect(evt){
    let files = evt.target.files;
    let file = files[0];

    if (files && file) {
      let reader = new FileReader();

      reader.onload =this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.addNewsModal.image = btoa(binaryString);
  }

  addNewMoreNews(): void {
  }

  openDescription(accordionId): void {
    this.accordions[accordionId] = !this.accordions[accordionId];
  }

  isDescription(accordionId): boolean {
    return this.accordions[accordionId];
  }

  getCredentials(): boolean {
    return !!sessionStorage.getItem("authToken");
  }
}
