import { Component, OnInit } from '@angular/core';
import Coach from "../../domain/Coach";
import {ApiService} from "../../services/api.service";
import News from "../../domain/News";

@Component({
  selector: 'app-coach-page',
  templateUrl: './coach-page.component.html',
  providers: [ApiService],
  styleUrls: ['./coach-page.component.css']
})
export class CoachPageComponent implements OnInit {
  title: string = 'ТРЕНЕРА';
  counter: number = 0;
  coachList: Array<Coach>;
  coachListT: Array<Coach>;
  masonList: Array<Coach> = [
    {
      name: 'Elon Musk',
      speciality: 'SpaceX & Tesla',
      img: 'https://www.biography.com/.image/t_share/MTE1ODA0OTcxOTUyMDE0ODYx/elon-musk-20837159-1-402.png',
      age: '∞',
      phone: '+380500542024',
      mail: 'marusiakvlad0011@gmail.com'
    },
    {
      name: 'Steve Jobs',
      speciality: 'Apple & Pixar',
      img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Steve_Jobs_Headshot_2010-CROP.jpg/1200px-Steve_Jobs_Headshot_2010-CROP.jpg',
      age: '∞',
      phone: '+380500542024',
      mail: 'marusiakvlad0011@gmail.com'
    },
    {
      name: 'Stephen Hawking',
      speciality: 'CH CBE FRS FRSA',
      img: 'https://www.biography.com/.image/t_share/MTI1OTA1ODM4NTg0OTI4NzM0/stephen-hawking-getty-imagesjpg.jpg',
      age: '∞',
      phone: '+380500542024',
      mail: 'marusiakvlad0011@gmail.com'
    },
    {
      name: 'Боронило Руслан',
      speciality: 'Підор',
      img: 'https://i.ytimg.com/vi/g_2qjClj-fk/maxresdefault.jpg',
      age: '-',
      phone: '+380500542024',
      mail: 'marusiakvlad0011@gmail.com'
    }
  ];

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.apiService.coaches().subscribe(
      data => {
        this.coachListT = data as Coach[];
        this.coachList = this.coachListT;
      },
      error => {},
      () => {}
    );
  }

  changeCoachList = function() {
    ++this.counter;
    if(this.counter === 5) {
      this.coachList = this.masonList;
      this.title = '# МАСОНИ';

      setTimeout(() => {
        this.coachList = this.coachListT;
        this.title = '# ТРЕНЕРА';
      }, 2000)
    }
  }
}
