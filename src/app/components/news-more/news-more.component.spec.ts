import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsMoreComponent } from './news-more.component';

describe('NewsMoreComponent', () => {
  let component: NewsMoreComponent;
  let fixture: ComponentFixture<NewsMoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsMoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsMoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
