import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'presidia-page',
  templateUrl: './presidia-page.component.html',
  styleUrls: ['./presidia-page.component.css']
})
export class PresidiaPageComponent implements OnInit {
  addPhotoModal = {
    open: false,
    image: null
  };

  constructor() {
  }

  ngOnInit() {
  }
}
