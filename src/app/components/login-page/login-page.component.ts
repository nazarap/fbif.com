import { Component, OnInit } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import {Router} from "@angular/router";
import {AuthService} from "./../../services/auth.service";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}, AuthService],
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  credentials: any = {
    login: '',
    password: ''
  };

  constructor(private location: Location, private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.credentials.login, this.credentials.password).subscribe(
      data => {
        console.log('login', data);
        sessionStorage.setItem("authToken", data.access_token);
        this.router.navigate(['/']);
      },
      error => {
        window.open('https://www.youtube.com/watch?v=TAUvZSJ_jyQ', '_blank');
      },
      () => {}
    );
  }
}
