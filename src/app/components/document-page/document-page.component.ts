import { Component, OnInit } from '@angular/core';
import { StyleService } from './../../services/style.service'
import Style from './../../domain/Style'
import ImageAbstract from './../../domain/ImageAbstract';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-document-page',
  templateUrl: './document-page.component.html',
  styleUrls: ['./document-page.component.css']
})
export class DocumentPageComponent implements OnInit {
  constructor() {
  }

  ngOnInit(): void {
  }

}
