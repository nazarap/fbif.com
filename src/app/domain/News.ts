export default class News {

  constructor(public title: string, public description: string, public image: string, public date?: any, public category?: string, public id?: any) {
  }

}
