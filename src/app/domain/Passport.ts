export default class Passport {

  constructor(public name: string, public age: any, public growth: string,
              public weight: string, public city: string, public team: string, public img: string) {
  }

}
