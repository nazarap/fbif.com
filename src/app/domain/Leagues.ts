import Team from "./Team";

export default class Leagues {

  constructor(public name: string, public id: number, public children: Array<Team>, public open?: boolean) {
  }

}
