import { Injectable } from '@angular/core';
import {AUTH, BASE_URL} from '../app.config';
import {Headers} from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import News from '../domain/News';
import Image from '../domain/Image';

@Injectable()
export class ApiService {
  headersGet: HttpHeaders;
  headersPost: HttpHeaders;
  constructor(private http: HttpClient) {
    this.headersGet = new HttpHeaders({'Authorization': AUTH});
    this.headersPost = new HttpHeaders({'Authorization': `bearer ${sessionStorage.getItem('authToken')}`});
  }

  news(): any {
    return this.http
      .get(`${BASE_URL}api/news`, { headers: this.headersGet });
  }

  newsGame(): any {
    return this.http
      .get(`${BASE_URL}api/news/category/GAME`, { headers: this.headersGet });
  }

  newsGeneral(): any {
    return this.http
      .get(`${BASE_URL}api/news/category/GENERAL`, { headers: this.headersGet });
  }

  advertisement(): any {
    return this.http
      .get(`${BASE_URL}api/advertisement`, { headers: this.headersGet });
  }

  sponsors(): any {
    return this.http
      .get(`${BASE_URL}api/sponsors`, { headers: this.headersGet });
  }

  addSponsors(sponsor: Image): any {
    return this.http
      .post(`${BASE_URL}api/sponsors`, sponsor, { headers: this.headersPost });
  }

  deleteSponsors(id: any): any {
    return this.http
      .delete(`${BASE_URL}api/sponsors/${id}`, { headers: this.headersPost });
  }

  coaches(): any {
    return this.http
      .get(`${BASE_URL}api/trainers`, { headers: this.headersGet });
  }

  passports(): any {
    return this.http
      .get(`${BASE_URL}api/passports`, { headers: this.headersGet });
  }

  videos(): any {
    return this.http
      .get(`${BASE_URL}api/videos`, { headers: this.headersGet });
  }

  photos(): any {
    return this.http
      .get(`${BASE_URL}api/cheerleaders`, { headers: this.headersGet });
  }

  addNews(news: News): any {
    return this.http
      .post(`${BASE_URL}api/news`, news, { headers: this.headersPost });
  }

  deleteNews(id: any): any {
    return this.http
      .delete(`${BASE_URL}api/news/${id}`, { headers: this.headersPost });
  }

  deleteSponsor(id: any): any {
    return this.http
      .delete(`${BASE_URL}api/sponsors/${id}`, { headers: this.headersPost });
  }

  addPhoto(image: Image): any {
    return this.http
      .post(`${BASE_URL}api/cheerleaders`, image, { headers: this.headersPost });
  }

  deletePhoto(id: any): any {
    return this.http
      .delete(`${BASE_URL}api/cheerleaders/${id}`, { headers: this.headersPost });
  }
}
