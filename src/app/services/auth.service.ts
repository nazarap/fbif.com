import { Injectable } from '@angular/core';
import { BASE_URL, AUTH } from '../app.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthService {
    private headers: HttpHeaders;

    constructor(private http: HttpClient) {
        this.headers = new HttpHeaders(
          {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': AUTH
          }
          );
    }

    login(login: string, password: string): any {
        const body = `grant_type=password&username=${login}&password=${password}`;
        return this.http
           .post(`${BASE_URL}oauth/token`, body, { headers: this.headers });
    }
}
