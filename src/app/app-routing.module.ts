import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainPageComponent } from './components/main-page/main-page.component';
import { T3x3PageComponent } from './components/t3x3-page/t3x3-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { CoachPageComponent } from './components/coach-page/coach-page.component';
import { CalendarPageComponent } from './components/calendar-page/calendar-page.component';
import { PassportPageComponent } from './components/passport-page/passport-page.component';
import { PhotoPageComponent } from './components/photo-page/photo-page.component';
import { TeamPageComponent } from './components/team-page/team-page.component';
import { VideoPageComponent } from './components/video-page/video-page.component';
import { PresidiaPageComponent } from './components/presidia-page/presidia-page.component';
import { DocumentPageComponent } from './components/document-page/document-page.component';

const routes: Routes = [
  { path: '', component: MainPageComponent },

  { path: '3x3', component: T3x3PageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'coach', component: CoachPageComponent },
  { path: 'news', component: CalendarPageComponent },
  { path: 'passport', component: PassportPageComponent },
  { path: 'photo', component: PhotoPageComponent },
  { path: 'team', component: TeamPageComponent },
  { path: 'video', component: VideoPageComponent },
  { path: 'presidia', component: PresidiaPageComponent },
  { path: 'documents', component: DocumentPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
